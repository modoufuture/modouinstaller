<?php
namespace modoufuture\installer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;
use Composer\Repository\InstalledRepositoryInterface;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;

class Addons extends LibraryInstaller
{
    protected $addon = [];
    protected $extra = [];
    protected $base = false;
    protected $root_extra = [];
    protected $root_path;
    protected $addon_path;
    protected $package;
    public static $command = [];

    public function install(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        parent::install($repo, $package);
        if ($this->isInProject()) {
            $this->execute($package, 'install');
        }
    }

    public function update(InstalledRepositoryInterface $repo, PackageInterface $initial, PackageInterface $target)
    {
        parent::update($repo, $initial, $target);
        if ($this->isInProject()) {
            $this->execute($target, 'update');
        }
    }

    public function uninstall(InstalledRepositoryInterface $repo, PackageInterface $package)
    {
        parent::uninstall($repo, $package);
        if ($this->isInProject()) {
            $this->execute($package, 'uninstall');
        }
    }

    protected function execute(PackageInterface $package, $type)
    {
        $this->init($package);
        static::$command[$type][] = ['name'=>$this->addon['name'], 'type'=>$this->addon['type'], 'base'=>$this->base];
        if ($type !== 'uninstall') {
            $this->copyToAddonPath();
        } else {
            if ($this->base) {
                return;
            }
        }
    }

    protected function init(PackageInterface $package)
    {
        $this->package = $package;
        $name = substr($package->getPrettyName(), 12);
        $this->addon = ['type'=>'', 'name'=>$name];
        $this->extra = $package->getExtra();
        $this->base = !empty($this->extra['base']);
        $this->root_extra = $this->composer->getPackage()->getExtra();
        $root_path = parent::getInstallPath($this->composer->getPackage());
        $this->root_path = dirname($root_path, 3);
        $addon_path = $root_path.'/'.$this->root_extra['addon_path'].'/'.$this->extra['type'].'/'.$name;
        $this->addon_path = str_replace(['//'], ['/'], $addon_path);
    }

    protected function isInProject()
    {
        return $this->composer->getPackage()->getType() == 'project';
    }

    protected function copyToAddonPath()
    {
        $package_path = parent::getInstallPath($this->package);
        if (!is_dir($this->addon_path)) {
            mkdir($this->addon_path, 0755, true);
        }
        // 复制目录到插件目录
        $this->copyFiles($package_path.'/src', $this->addon_path);
    }

    protected function copyFiles($src, $dest)
    {
        // 遍历自己的目录
        $dirs = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($src, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST
        );
        foreach($dirs as $k=>$dir) {
            $path = $dirs->getSubPathName();
            if ($dir->isDir() && !is_dir($dest.'/'.$path)) {
                mkdir($dest.'/'.$path, 0755, true);
            } elseif(!$dir->isDir()) {
                copy($dir->getRealPath(), $dest.'/'.$path);
            }
        }
    }

    public function supports($packageType)
    {
        return 'modouthink-addon' === $packageType;
    }
}