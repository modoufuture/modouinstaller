<?php

namespace modoufuture\installer;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginEvents;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;

class Plugin implements PluginInterface,EventSubscriberInterface
{
    /**
     * @var Composer
     */
    protected $composer;

    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $manager = $composer->getInstallationManager();
        // 模块安装
        $manager->addInstaller(new Addons($io, $composer));
    }

    public static function getSubscribedEvents()
    {
        return [
            ScriptEvents::POST_INSTALL_CMD => [
                ['onPostInstallCmd', 0]
            ],
            ScriptEvents::POST_UPDATE_CMD => [
                ['onPostInstallCmd', 0]
            ]
        ];
    }

    public function onPostInstallCmd(Event $event)
    {
        $extra =$this->composer->getPackage()->getExtra();
        $cmd = !empty($extra['cmd']) ? $extra['cmd'] : [];
        if (!empty($cmd)) {
            require $cmd['class'];
            call_user_func_array($cmd['call'], ['event'=>$event, 'addons'=>Addons::$command]);
        }
    }
}